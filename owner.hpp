/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

namespace pizza
{

template <class T>
class owner {
public:
    owner(T* ptr = nullptr) noexcept : ptr_(ptr) {}

    constexpr bool has_owner() const noexcept { return ptr_ != nullptr; }
    constexpr void set_owner(T* ptr) { ptr_ = ptr; }
    constexpr const T* get_owner() const noexcept { return ptr_; }
    constexpr T* get_owner() noexcept { return ptr_; }

private:
    T* ptr_;
};

}
