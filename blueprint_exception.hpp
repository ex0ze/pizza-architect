/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include <exception>
#include <string>
#include <vector>

namespace pizza
{

class blueprint_exception : public std::exception {
public:
    enum class code {
        unspecified,
        undefined_format,
        missing_member,
        member_of_wrong_type,
        unknown_broker,
        unknown_bus
    };
    blueprint_exception(code c, std::string what, std::vector<std::string> && stacktrace);
    virtual const char * what() const noexcept override;
private:
    std::vector<std::string> stacktrace_;
    std::string what_;
    code code_ = code::unspecified;

    mutable std::string buf_;
};

}
