/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#pragma once

#include "owner.hpp"
#include <pizza-config/service.hpp>

namespace pizza {

class blueprint;

class ext_service : public service, public owner<blueprint> {
public:
    void set_balancer_name(std::string balancer_name) noexcept {
        balancer_name_ = std::move(balancer_name);
        has_balancer_name_ = true;
    }
    constexpr const std::string& balancer_name() const noexcept { return balancer_name_; }

    void set_broker_name(std::string broker_name) noexcept {
        broker_name_ = std::move(broker_name);
        has_broker_name_ = true;
    }
    constexpr const std::string& broker_name() const noexcept { return broker_name_; }

    void set_bus_name(std::string bus_name) noexcept {
        bus_name_ = std::move(bus_name);
        has_bus_name_ = true;
    }
    constexpr const std::string& bus_name() const noexcept { return bus_name_; }

    constexpr bool has_balancer_name() const noexcept { return has_balancer_name_; }
    constexpr bool has_broker_name() const noexcept { return has_broker_name_; }
    constexpr bool has_bus_name() const noexcept { return has_bus_name_; }

private:
    std::string balancer_name_;
    std::string broker_name_;
    std::string bus_name_;

    bool has_balancer_name_ = false;
    bool has_broker_name_ = false;
    bool has_bus_name_ = false;
};

};

