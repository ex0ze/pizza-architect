/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "blueprint_exception.hpp"

#include <sstream>

namespace
{

const char * code_to_string(pizza::blueprint_exception::code c) noexcept {
    using code = pizza::blueprint_exception::code;
    switch (c) {
        case code::undefined_format: return "undefined file format";
        case code::missing_member: return "missing member";
        case code::member_of_wrong_type: return "member of wrong type";
        case code::unknown_broker: return "can't find specified broker in config";
        case code::unknown_bus: return "can't find specified bus in config";
        case code::unspecified: default: return "unknown error";
    }
}

void print_stacktrace(const std::vector<std::string>& stacktrace, std::ostream& out) {
    for (const auto& frame : stacktrace)
        out << "At [" << frame << "]\n";
}

}

namespace pizza
{

blueprint_exception::blueprint_exception(code c, std::string what, std::vector<std::string> && stacktrace)
: code_(c), what_(std::move(what)), stacktrace_(std::move(stacktrace)) {}

const char * blueprint_exception::what() const noexcept {
    if (!buf_.empty()) return buf_.c_str();
    std::ostringstream stream;
    stream << code_to_string(code_) << ": " << what_ << "\n";
    print_stacktrace(stacktrace_, stream);
    buf_ = stream.str();
    return buf_.c_str();
}



} // namespace pizza
